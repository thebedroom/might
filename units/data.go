//
// filename:  data.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
// desc:      <short desc>
//

package iuc

type Data Unit
type DataSi Data
type DataByte Data

const (
	Byte Data = 1
	Kb        = 1000 * DataSi(Byte)
	Mb        = 1000 * Kb
	Gb        = 1000 * Mb
	Tb        = 1000 * Gb
	Pb        = 1000 * Tb
	Eb        = 1000 * Pb

	Kibit DataByte = 1024
	Mibit          = 1048576
	Gibit          = 1073741824
	Tibit          = 1099511627776
	Pibit          = 1125899906842624
	Eibit          = 1152921504606846976
)
