### Might v0.0.6

Features added : 
- [x] Loading of `.yml` and `.yaml` files.
- [x] Loading of `.xml` files.
- [x] Env getters for `time.Duration` 
- [x] Env getters for `time.Time`
- [x] Overrided the bool parser to add `yes` and `no` values. 