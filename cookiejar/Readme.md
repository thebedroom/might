# Incredible CookieJar

This package is a fork of the golang source `net/http/cookiejar` package.

It allows the package to be persistent in time. You can save the state at a specific time, export it and reuse it later.

> Please note that the content of the package is not modified unless the internal/ascii import

> Also note that the modifications are done under `addings.go`