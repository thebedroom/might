package cookiejar

import (
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"sort"
	"time"
)

type Marshaler interface {
	Encode(v interface{}) error
}

type Unmarshaler interface {
	Decode(v interface{}) error
}

func (j *Jar) ExportJsonByUrl(u *url.URL, writer io.Writer) error {
	cookies := j.cookiesByUrl(u)

	enc := json.NewEncoder(writer)

	return enc.Encode(cookies)
}

func (j *Jar) ExportJson(writer io.Writer) error {
	enc := json.NewEncoder(writer)

	return j.Export(enc)
}

func (j *Jar) Export(encoder Marshaler) error {
	cookies := j.filterCookiesExpired(time.Now())

	return encoder.Encode(cookies)
}

func (j *Jar) ImportJsonByUrl(u *url.URL, reader io.Reader) error {
	dec := json.NewDecoder(reader)

	_, key := j.cookieKey(u)

	entries := j.entries[key]

	return dec.Decode(&entries)
}

func (j *Jar) ImportJson(reader io.Reader) error {
	dec := json.NewDecoder(reader)

	return j.Import(dec)
}

func (j *Jar) Import(decoder Unmarshaler) error {
	return decoder.Decode(&j.entries)
}

func (j *Jar) UnsafeGetCookies(url *url.URL) []*http.Cookie {
	entries := j.unsafeCookies(url, time.Now())
	ret := make([]*http.Cookie, len(entries))

	for i, e := range entries {
		ret[i] = &http.Cookie{
			Name:     e.Name,
			Value:    e.Value,
			Path:     e.Path,
			Domain:   e.Domain,
			Expires:  e.Expires,
			Secure:   e.Secure,
			HttpOnly: e.HttpOnly,
		}

		switch e.SameSite {
		case "SameSite":
			ret[i].SameSite = http.SameSiteDefaultMode
		case "SameSite=Strict":
			ret[i].SameSite = http.SameSiteStrictMode
		case "SameSite=Lax":
			ret[i].SameSite = http.SameSiteLaxMode
		default:
			ret[i].SameSite = http.SameSiteNoneMode
		}
	}

	return ret
}

// region=helpers
func (j *Jar) cookiesByUrl(u *url.URL) []entry {
	return j.unsafeCookies(u, time.Now())
}

func (j *Jar) filterCookiesExpired(now time.Time) (cookies map[string]map[string]entry) {

	cookies = make(map[string]map[string]entry, len(j.entries))

	j.mu.Lock()
	defer j.mu.Unlock()

	for key, submap := range j.entries {
		if submap == nil {
			continue
		}

		modified := false
		var selected []entry
		for id, e := range submap {
			if e.Persistent && !e.Expires.After(now) {
				delete(submap, id)
				modified = true
				continue
			}
			e.LastAccess = now
			submap[id] = e
			selected = append(selected, e)
			modified = true
		}
		if modified {
			if len(submap) == 0 {
				delete(j.entries, key)
			} else {
				j.entries[key] = submap
			}
		}

		cookies[key] = submap
	}

	return
}

func (j *Jar) cookieKey(u *url.URL) (string, string) {
	if u.Scheme != "http" && u.Scheme != "https" {
		return "", ""
	}
	host, err := canonicalHost(u.Host)
	if err != nil {
		return "", ""
	}
	return host, jarKey(host, j.psList)
}

func (j *Jar) unsafeCookies(u *url.URL, now time.Time) (cookies []entry) {
	if u.Scheme != "http" && u.Scheme != "https" {
		return cookies
	}
	host, err := canonicalHost(u.Host)
	if err != nil {
		return cookies
	}
	key := jarKey(host, j.psList)

	j.mu.Lock()
	defer j.mu.Unlock()

	submap := j.entries[key]
	if submap == nil {
		return cookies
	}

	https := u.Scheme == "https"
	path := u.Path
	if path == "" {
		path = "/"
	}

	modified := false
	var selected []entry
	for id, e := range submap {
		if e.Persistent && !e.Expires.After(now) {
			delete(submap, id)
			modified = true
			continue
		}
		if !e.shouldSend(https, host, path) {
			continue
		}
		e.LastAccess = now
		submap[id] = e
		selected = append(selected, e)
		modified = true
	}
	if modified {
		if len(submap) == 0 {
			delete(j.entries, key)
		} else {
			j.entries[key] = submap
		}
	}

	// sort according to RFC 6265 section 5.4 point 2: by longest
	// path and then by earliest creation time.
	sort.Slice(selected, func(i, j int) bool {
		s := selected
		if len(s[i].Path) != len(s[j].Path) {
			return len(s[i].Path) > len(s[j].Path)
		}
		if !s[i].Creation.Equal(s[j].Creation) {
			return s[i].Creation.Before(s[j].Creation)
		}
		return s[i].seqNum < s[j].seqNum
	})

	return selected
}

// endregion
