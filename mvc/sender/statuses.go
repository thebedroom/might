// filename:  statuses.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
package irs

import (
	"encoding/json"
	"net/http"
	"strconv"
)

type Status struct {
	HttpCode int
	Message  string
}

type ResponseStatusMap map[Code]Status

var ResponseStatuses = ResponseStatusMap{
	Ok:                   {http.StatusOK, "SUCCESS.OK"},
	Created:              {http.StatusCreated, "SUCCESS.CREATED"},
	Accepted:             {http.StatusAccepted, "SUCCESS.ACCEPTED"},
	NonAuthoritativeInfo: {http.StatusNonAuthoritativeInfo, "SUCCESS.NON_AUTHORITATIVE_INFO"},
	NoContent:            {http.StatusNoContent, "SUCCESS.NO_CONTENT"},
	ResetContent:         {http.StatusResetContent, "SUCCESS.RESET_CONTENT"},
	PartialContent:       {http.StatusPartialContent, "SUCCESS.PARTIAL_CONTENT"},
	MultiStatus:          {http.StatusMultiStatus, "SUCCESS.MULTI_STATUS"},
	AlreadyReported:      {http.StatusAlreadyReported, "SUCCESS.ALREADY_REPORTED"},
	IMUsed:               {http.StatusIMUsed, "SUCCESS.IMUSED"},

	BadRequest:                   {http.StatusBadRequest, "ERRORS.REQUEST.BAD_REQUEST"},
	Unauthorized:                 {http.StatusUnauthorized, "ERRORS.REQUEST.UNAUTHORIZED"},
	PaymentRequired:              {http.StatusPaymentRequired, "ERRORS.REQUEST.PAYMENT_REQUIRED"},
	Forbidden:                    {http.StatusForbidden, "ERRORS.REQUEST.FORBIDDEN"},
	NotFound:                     {http.StatusNotFound, "ERRORS.REQUEST.NOT_FOUND"},
	MethodNotAllowed:             {http.StatusMethodNotAllowed, "ERRORS.REQUEST.METHOD_NOT_ALLOWED"},
	NotAcceptable:                {http.StatusNotAcceptable, "ERRORS.REQUEST.NOT_ACCEPTABLE"},
	ProxyAuthRequired:            {http.StatusProxyAuthRequired, "ERRORS.REQUEST.PROXY_AUTH_REQUIRED"},
	RequestTimeout:               {http.StatusRequestTimeout, "ERRORS.REQUEST.REQUEST_TIMEOUT"},
	Conflict:                     {http.StatusConflict, "ERRORS.REQUEST.CONFLICT"},
	Gone:                         {http.StatusGone, "ERRORS.REQUEST.GONE"},
	LengthRequired:               {http.StatusLengthRequired, "ERRORS.REQUEST.LENGTH_REQUIRED"},
	PreconditionFailed:           {http.StatusPreconditionFailed, "ERRORS.REQUEST.PRECONDITION_FAILED"},
	RequestEntityTooLarge:        {http.StatusRequestEntityTooLarge, "ERRORS.REQUEST.REQUEST_ENTITY_TOO_LARGE"},
	RequestURITooLong:            {http.StatusRequestURITooLong, "ERRORS.REQUEST.REQUEST_URI_TOO_LONG"},
	UnsupportedMediaType:         {http.StatusUnsupportedMediaType, "ERRORS.REQUEST.UNSUPPORTED_MEDIA_TYPE"},
	RequestedRangeNotSatisfiable: {http.StatusRequestedRangeNotSatisfiable, "ERRORS.REQUEST.REQUESTED_RANGE_NOT_SATISFIABLE"},
	ExpectationFailed:            {http.StatusExpectationFailed, "ERRORS.REQUEST.EXPECTATION_FAILED"},
	Teapot:                       {http.StatusTeapot, "ERRORS.REQUEST.TEAPOT"},
	MisdirectedRequest:           {http.StatusMisdirectedRequest, "ERRORS.REQUEST.MISDIRECTED_REQUEST"},
	UnprocessableEntity:          {http.StatusUnprocessableEntity, "ERRORS.REQUEST.UNPROCESSABLE_ENTITY"},
	Locked:                       {http.StatusLocked, "ERRORS.REQUEST.LOCKED"},
	FailedDependency:             {http.StatusFailedDependency, "ERRORS.REQUEST.FAILED_DEPENDENCY"},
	TooEarly:                     {http.StatusTooEarly, "ERRORS.REQUEST.TOO_EARLY"},
	UpgradeRequired:              {http.StatusUpgradeRequired, "ERRORS.REQUEST.UPGRADE_REQUIRED"},
	PreconditionRequired:         {http.StatusPreconditionRequired, "ERRORS.REQUEST.PRECONDITION_REQUIRED"},
	TooManyRequests:              {http.StatusTooManyRequests, "ERRORS.REQUEST.TOO_MANY_REQUEST"},
	RequestHeaderFieldsTooLarge:  {http.StatusRequestHeaderFieldsTooLarge, "ERRORS.REQUEST.REQUEST_HEADER_FIELDS_TOO_LARGE"},
	UnavailableForLegalReasons:   {http.StatusUnavailableForLegalReasons, "ERRORS.REQUEST.UNAVAILABLE_FOR_LEGAL_REASONS"},

	InternalServerError:           {http.StatusInternalServerError, "ERRORS.SERVER.INTERNAL_SERVER_ERROR"},
	NotImplemented:                {http.StatusNotImplemented, "ERRORS.SERVER.NOT_IMPLEMENTED"},
	BadGateway:                    {http.StatusBadGateway, "ERRORS.SERVER.BAD_GATEWAY"},
	ServiceUnavailable:            {http.StatusServiceUnavailable, "ERRORS.SERVER.SERVICE_UNAVAILABLE"},
	GatewayTimeout:                {http.StatusGatewayTimeout, "ERRORS.SERVER.GATEWAY_TIMEOUT"},
	HTTPVersionNotSupported:       {http.StatusHTTPVersionNotSupported, "ERRORS.SERVER.HTTP_VERSION_NOT_SUPPORTED"},
	VariantAlsoNegotiates:         {http.StatusVariantAlsoNegotiates, "ERRORS.SERVER.VARIANT_ALSO_NEGOTIATES"},
	InsufficientStorage:           {http.StatusInsufficientStorage, "ERRORS.SERVER.INSUFFICIENT_STORAGE"},
	LoopDetected:                  {http.StatusLoopDetected, "ERRORS.SERVER.LOOP_DETECTED"},
	NotExtended:                   {http.StatusNotExtended, "ERRORS.SERVER.NOT_EXTENDED"},
	NetworkAuthenticationRequired: {http.StatusNetworkAuthenticationRequired, "ERRORS.SERVER.NETWORK_AUTHENTICATION_REQUIRED"},
}

func (i ResponseStatusMap) MarshalJSON() ([]byte, error) {
	x := make(map[string]Status)
	for k, v := range i {
		x[strconv.FormatInt(int64(k), 10)] = v
	}
	return json.Marshal(x)
}

func (i *ResponseStatusMap) UnmarshalJSON(b []byte) error {
	x := make(map[string]Status)
	if err := json.Unmarshal(b, &x); err != nil {
		return err
	}
	*i = make(ResponseStatusMap, len(x))
	for k, v := range x {
		if ki, err := strconv.ParseInt(k, 10, 32); err != nil {
			return err
		} else {
			(*i)[Code(ki)] = v
		}
	}
	return nil
}

func AddStatus(code Code, httpCode int, message string) {
	ResponseStatuses[code] = Status{
		httpCode,
		message,
	}
}
