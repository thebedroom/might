// filename:  error.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
package irsa

import (
	"fmt"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"

	"gitlab.com/thebedroom/might/mvc/sender"
)

type errorResponseTemplate struct {
	ErrCode   irs.Code    `json:"errCode" xml:"errCode"`
	Message   string      `json:"message" xml:"message"`
	Details   interface{} `json:"details,omitempty" xml:"details,omitempty"`
	RequestId string      `json:"requestId,omitempty" xml:"requestId,omitempty"`
}

func SendError(c echo.Context, err error) error {
	id := uuid.NewString()

	e := irs.HttpError{
		HttpCode: http.StatusInternalServerError,
		ErrCode:  0,
		Message:  http.StatusText(http.StatusInternalServerError),
	}
	var details interface{}

	if cde, ok := err.(irs.CodeError); ok {
		e.Message = cde.Message()
		e.ErrCode = cde.Code
		e.HttpCode = cde.HttpCode()
		c.Logger().Error("id="+id, fmt.Sprint(cde.Err))
		c.Response().Header().Set("X-Error-Id", id)
	} else if cde, ok := err.(*irs.CodeError); ok {
		e.Message = cde.Message()
		e.ErrCode = cde.Code
		e.HttpCode = cde.HttpCode()
		c.Logger().Error("id="+id, fmt.Sprint(cde.Err))
		c.Response().Header().Set("X-Error-Id", id)
	} else if cd, ok := err.(irs.Code); ok {
		e.Message = cd.Message()
		e.ErrCode = cd
		e.HttpCode = cd.HttpCode()
	} else if cd, ok := err.(*irs.Code); ok {
		e.Message = cd.Message()
		e.ErrCode = *cd
		e.HttpCode = cd.HttpCode()
	} else if he, ok := err.(*echo.HTTPError); ok {
		code := irs.Code(he.Code)
		e.HttpCode = code.HttpCode()
		e.ErrCode = code
		e.Message = code.Message()
	} else if se, ok := err.(*irs.ServerError); ok {
		c.Logger().Error("id="+id, fmt.Sprint(se.Err))
		c.Response().Header().Set("X-Error-Id", id)
		e = se.HttpError
	} else if se, ok := err.(irs.ServerError); ok {
		c.Logger().Error("id="+id, fmt.Sprint(se.Err))
		c.Response().Header().Set("X-Error-Id", id)
		e = se.HttpError
	} else if he, ok := err.(*irs.HttpError); ok {
		e = *he
	} else if he, ok := err.(irs.HttpError); ok {
		e = he
	} else if ve, ok := err.(validator.ValidationErrors); ok {
		e.HttpCode = irs.BadRequest.HttpCode()
		e.ErrCode = irs.BadRequest
		e.Message = irs.BadRequest.Message()
		details = transformValidatorErrors(ve)
	} else {
		c.Logger().Error("id="+id, fmt.Sprint(err))
		c.Response().Header().Set("X-Error-Id", id)
	}

	return RawSend(c, e.HttpCode, errorResponseTemplate{
		ErrCode: e.ErrCode,
		Message: e.Message,
		Details: details,
	})
}

func ErrorHandler(err error, ctx echo.Context) {
	e := SendError(ctx, err)
	if e != nil {
		ctx.Logger().Fatal(err)
	}
}

func transformValidatorErrors(ve validator.ValidationErrors) interface{} {
	ret := make([]map[string]interface{}, len(ve))

	for k, v := range ve {
		ret[k] = map[string]interface{}{
			"field":     v.Field(),
			"problem":   v.ActualTag(),
			"expecting": v.Param(),
			"got":       v.Value(),
		}
	}

	return ret
}
