//
// filename:  base.go.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package managers

import (
	"reflect"

	"gitlab.com/thebedroom/might/mvc/repositories"
)

type Manager interface {
	Id() string
	Init(collection *repositories.Collection, managers *Collection)
	AfterInit() error
}

type BaseManager struct {
}

func (m *BaseManager) Id() string {
	return reflect.TypeOf(m).Name()
}

func (m *BaseManager) Init(collection *repositories.Collection, managers *Collection) {

}

func (m *BaseManager) AfterInit() error {
	return nil
}
