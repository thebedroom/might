//
// filename:  collection.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package managers

import (
	"reflect"

	"github.com/sirupsen/logrus"

	imvcRepositories "gitlab.com/thebedroom/might/mvc/repositories"
)

type Collection struct {
	_repo map[string]Manager
}

func NewCollection() *Collection {
	return &Collection{
		_repo: map[string]Manager{},
	}
}

func (r *Collection) DoInit(repos *imvcRepositories.Collection, managers *Collection) {
	for _, v := range r._repo {
		v.Init(repos, managers)
	}
}

func (r *Collection) DoAfterInit() error {
	for _, v := range r._repo {
		if err := v.AfterInit(); err != nil {
			return err
		}
	}

	return nil
}

func (r *Collection) Get(key interface{}) interface{} {
	if manager, ok := key.(Manager); ok {
		key := manager.Id()
		if key == "" {
			t := reflect.TypeOf(manager)
			if t.Kind() == reflect.Ptr {
				t = t.Elem()
			}
			key = t.Name()
		}
		return r.GetName(key)
	}
	return r.GetName(reflect.TypeOf(key).Name())
}

func (r *Collection) GetName(key string) interface{} {
	if val, ok := r._repo[key]; ok {
		return val
	}

	logrus.Panic("The managers", key, "does not exist")
	panic("")
}

func (r *Collection) Set(item Manager) *Collection {
	key := item.Id()
	if key == "" {
		t := reflect.TypeOf(item)
		if t.Kind() == reflect.Ptr {
			t = t.Elem()
		}
		key = t.Name()
	}

	r._repo[key] = item

	return r
}
