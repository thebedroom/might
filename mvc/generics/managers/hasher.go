//
// filename:  hasher.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package managers

import (
	"bytes"
	"crypto/md5"
	"crypto/rand"
	"encoding/base64"
	"reflect"

	"gitlab.com/thebedroom/might/mvc/managers"
	irs "gitlab.com/thebedroom/might/mvc/sender"
	"golang.org/x/crypto/pbkdf2"
	"golang.org/x/crypto/sha3"
)

// / The type to Identify the Basic Hasher Identifiers
type BaseHasherIdentifiers int

const (
	HasherBase BaseHasherIdentifiers = iota
	HasherMD5
	HasherSHA256
	HasherSHA512
	HasherPBKDFSHA512

	HasherFastest   = HasherBase
	HasherStrongest = HasherPBKDFSHA512
)

func (b BaseHasherIdentifiers) GetMagicTag() [4]byte {
	switch b {
	case HasherBase:
		return [4]byte{0xde, 0xad, 0xbe, 0xef}
	case HasherMD5:
		return [4]byte{0xC0, 0x00, 0x10, 0xFF}
	case HasherSHA256:
		return [4]byte{}
	case HasherSHA512:
		return [4]byte{0x0D, 0x15, 0xEA, 0x5E}
	case HasherPBKDFSHA512:
		return [4]byte{0x0D, 0x06, 0xF0, 0x0D}
	}
	panic("unknown base hasher")
}

func (b BaseHasherIdentifiers) GetSaltLen(_ []byte) int {
	switch b {
	case HasherBase:
		return 16
	case HasherMD5:
		return 32
	case HasherSHA512:
		return 64
	case HasherPBKDFSHA512:
		return 128
	}
	panic("unknown base hasher")
}

func (b BaseHasherIdentifiers) DoHash(toHash, salt []byte) []byte {
	switch b {
	case HasherBase:
	case HasherMD5:
		sum := md5.Sum(append(salt, toHash...))
		return sum[0:]
	case HasherSHA256:
		return sha3.New256().Sum(append(salt, toHash...))
	case HasherSHA512:
		return sha3.New512().Sum(append(salt, toHash...))
	case HasherPBKDFSHA512:
		return pbkdf2.Key(toHash, salt, 32768, 512, sha3.New512)
	}
	panic("unknown base hasher")
}

func (b BaseHasherIdentifiers) Match(hash, salt, input []byte) (bool, error) {
	var sum []byte

	switch b {
	case HasherBase:
	case HasherMD5:
		toSlice := md5.Sum(append(salt, input...))
		sum = toSlice[0:]
	case HasherSHA256:
		sum = sha3.New256().Sum(append(salt, input...))
	case HasherSHA512:
		sum = sha3.New512().Sum(append(salt, input...))
	case HasherPBKDFSHA512:
		sum = pbkdf2.Key(input, salt, 32768, 512, sha3.New512)
	default:
		panic("unknown base hasher")
	}

	return bytes.Compare(sum, hash) == 0, nil
}

type HasherVersion interface {
	GetMagicTag() [4]byte
	GetSaltLen(toHash []byte) int
	DoHash(toHash, salt []byte) []byte
	Match(hash, salt, input []byte) (bool, error)
}

type HasherManager struct {
	managers.BaseManager

	baseHashers map[[4]byte]HasherVersion
	UserHashers map[[4]byte]HasherVersion
}

func (h *HasherManager) Id() string {
	return reflect.TypeOf(HasherManager{}).Name()
}

func (h *HasherManager) setVersion(hash []byte, version HasherVersion) []byte {
	ret := make([]byte, 0, len(hash)+4)

	tag := version.GetMagicTag()
	ret = append(ret, tag[0:]...)
	ret = append(ret, hash...)

	return ret
}

func (h *HasherManager) getVersion(hashWithVersion []byte) (magicTag [4]byte, hash []byte) {

	for i := 0; i < 4; i++ {
		magicTag[i] = hashWithVersion[i]
	}

	hash = hashWithVersion[4:]
	return
}

func (h *HasherManager) Hash(toHash []byte, version HasherVersion) (hash []byte, salt []byte, err error) {
	saltLen := version.GetSaltLen(toHash)
	salt = make([]byte, saltLen)
	if _, err = rand.Read(salt); err != nil {
		panic(err)
	}

	hash = version.DoHash(toHash, salt)

	hash = h.setVersion(hash, version)

	return
}

func (h *HasherManager) HashB64(toHash []byte, version HasherVersion) (hash, salt string, err error) {
	ha, sa, err := h.Hash(toHash, version)

	if err != nil {
		return "", "", err
	}

	enc := base64.StdEncoding

	return enc.EncodeToString(ha), enc.EncodeToString(sa), nil
}

func (h *HasherManager) Verify(hash, salt, input []byte, version HasherVersion) (bool, error) {
	return version.Match(hash, salt, input)
}

func (h *HasherManager) VerifyB64(hash, salt string, input []byte, version HasherVersion) (bool, error) {
	dec := base64.StdEncoding

	ha, err := dec.DecodeString(hash)
	if err != nil {
		return false, err
	}

	ver, ha := h.getVersion(ha)
	if ver != version.GetMagicTag() {
		return false, irs.InternalServerError
	}

	sa, err := dec.DecodeString(salt)
	if err != nil {
		return false, err
	}

	return h.Verify(ha, sa, input, version)
}
