//
// filename:  pagination.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package generics

type Page struct {
	Pagination `groups:"page"`
	Total      int64       `json:"total"   xml:"total"   groups:"page"`
	Content    interface{} `json:"content" xml:"content" groups:"page"`
}

type Pagination struct {
	Page int64 `query:"page" form:"page" param:"page" json:"page" xml:"page" groups:"page"`
	Size int64 `query:"size" form:"size" param:"size" json:"size" xml:"size" groups:"page"`
}

type PaginationOption struct {
	DefaultPage *int64
	DefaultSize *int64
}
