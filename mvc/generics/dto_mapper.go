//
// filename:  dto_mapper.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package generics

type DTOMapper interface {
	ToODT(outType interface{}) error
}
