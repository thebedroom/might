//
// filename:  base.go.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package repositories

import (
	"reflect"
)

type Repository interface {
	Id() string
	Init(connector interface{})
}

type BaseRepository struct {
}

func (m *BaseRepository) Id() string {
	return reflect.TypeOf(m).Name()
}

func (r *BaseRepository) Init(connector interface{}) {

}
