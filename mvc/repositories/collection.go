//
// filename:  collection.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package repositories

import (
	"reflect"

	"github.com/sirupsen/logrus"
)

type Collection struct {
	_repo map[string]Repository
}

func NewCollection() *Collection {
	return &Collection{
		_repo: map[string]Repository{},
	}
}

func (r *Collection) Init(orm interface{}) {
	for _, v := range r._repo {
		v.Init(orm)
	}
}

func (r *Collection) Get(key interface{}) interface{} {
	if manager, ok := key.(Repository); ok {
		key := manager.Id()
		if key == "" {
			t := reflect.TypeOf(manager)
			if t.Kind() == reflect.Ptr {
				t = t.Elem()
			}
			key = t.Name()
		}
		return r.GetName(key)
	}
	return r.GetName(reflect.TypeOf(key).Name())
}

func (r *Collection) GetName(key string) interface{} {
	if val, ok := r._repo[key]; ok {
		return val
	}

	logrus.Panic("The repository", key, "does not exist")
	panic("")
}

func (r *Collection) Set(item Repository) *Collection {

	key := item.Id()
	if key == "" {
		t := reflect.TypeOf(item)
		if t.Kind() == reflect.Ptr {
			t = t.Elem()
		}
		key = t.Name()
	}

	r._repo[key] = item

	return r
}
