//
// filename:  pagination.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package controllers

import (
	"github.com/labstack/echo/v4"

	"gitlab.com/thebedroom/might/mvc/generics"
)

var defaultPagination = generics.Pagination{
	Page: 1,
	Size: 20,
}

func (b *BaseController) Pagination(ctx echo.Context) (*generics.Pagination, error) {
	p := b.DefaultPagination

	if err := b.Bind(ctx, &p); err != nil {
		return nil, err
	}

	return &p, nil
}

func (b *BaseController) PaginationOption(ctx echo.Context, opts ...generics.PaginationOption) (*generics.Pagination, error) {

	p := b.DefaultPagination

	for _, o := range opts {
		if o.DefaultSize != nil {
			p.Size = *o.DefaultSize
		}
		if o.DefaultPage != nil {
			p.Page = *o.DefaultPage
		}
	}

	if err := b.Bind(ctx, &p); err != nil {
		return nil, err
	}

	return &p, nil
}
