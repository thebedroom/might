//
// filename:  collection.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package controllers

import (
	"reflect"
	"strings"

	"github.com/labstack/echo/v4"

	"gitlab.com/thebedroom/might/mvc/managers"
)

type Collection struct {
	options []BaseControllerOptions
	_repo   map[string]Controller
}

func NewCollection() *Collection {
	return &Collection{
		_repo: map[string]Controller{},
	}
}

func (r *Collection) Init() {
	for _, v := range r._repo {
		v.Init(r.options...)
	}
}

func (r *Collection) InitManagers(collection *managers.Collection) {
	for _, v := range r._repo {
		v.InitManagers(collection)
	}
}

func (r *Collection) Add(item ...Controller) *Collection {

	for _, controller := range item {
		name := ""

		root := controller.RootPath()
		if root == nil {
			name = reflect.TypeOf(controller).Elem().Name()
			name = strings.ToLower(name)
			name = strings.TrimSuffix(name, "controller")
		} else {
			name = *root
		}

		r._repo[name] = controller
	}

	return r
}

func (r *Collection) ApplyRouter(router *echo.Group) {
	for k, v := range r._repo {
		g := router.Group(k)
		v.ApplyRouter(g)
	}
}
