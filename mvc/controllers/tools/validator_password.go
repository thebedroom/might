//
// filename:  validator_password.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package tools

import (
	"strconv"
	"strings"
	"unicode"

	"github.com/go-playground/validator/v10"
)

func validatorPassword(fl validator.FieldLevel) bool {
	requiredLowers := -1
	requiredUppers := -1
	requiredDigits := -1
	requiredSymbol := -1

	params := strings.Split(fl.Param(), "&")
	for _, v := range params {
		value := 1

		idx := strings.Index(v, ":")
		if idx == -1 {
			if v, err := strconv.ParseInt(v[idx:], 10, 32); err != nil {
				panic(err)
			} else {
				value = int(v)
			}
		}

		switch strings.ToLower(v[:idx]) {
		case "lower":
			requiredLowers = value
			break
		case "upper":
			requiredUppers = value
			break
		case "digit":
			requiredDigits = value
			break
		case "symbol":
			requiredSymbol = value
			break
		}
	}

	for _, r := range fl.Field().String() {
		if unicode.IsDigit(r) && requiredDigits > 0 {
			requiredDigits--
		} else if unicode.IsUpper(r) && requiredUppers > 0 {
			requiredUppers--
		} else if unicode.IsLower(r) && requiredLowers > 0 {
			requiredLowers--
		} else if !unicode.IsLetter(r) && !unicode.IsDigit(r) && unicode.IsPrint(r) && requiredSymbol > 0 {
			requiredSymbol--
		}
	}

	if requiredDigits > 0 || requiredUppers > 0 || requiredLowers > 0 || requiredSymbol > 0 {
		return false
	}
	return true
}
