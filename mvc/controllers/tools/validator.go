//
// filename:  validator.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package tools

import (
	"github.com/go-playground/validator/v10"
)

type StructValidator struct {
	validator *validator.Validate
}

func (s *StructValidator) Validate(i interface{}) error {
	return s.validator.Struct(i)
}

func (s *StructValidator) AddValidator(tag string, validator validator.Func) error {
	if err := s.validator.RegisterValidation(tag, validator); err != nil {
		return err
	}

	return nil
}

var DefaultStructValidator *StructValidator

func init() {
	DefaultStructValidator = &StructValidator{validator: validator.New()}
	DefaultStructValidator.AddValidator("password", validatorPassword)
}
