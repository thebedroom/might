//
// filename:  base.go.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package controllers

import (
	"net/http"

	"github.com/liip/sheriff"

	"gitlab.com/thebedroom/might/mvc/generics"
	"gitlab.com/thebedroom/might/mvc/managers"
	irs "gitlab.com/thebedroom/might/mvc/sender"
	irsa "gitlab.com/thebedroom/might/mvc/sender/api"

	"github.com/labstack/echo/v4"
)

type BaseControllerOptions struct {
	BindDefaultError irs.Coder
	DataWrapper      func(ctx echo.Context, code irs.Code, data interface{}) interface{}
}

type Controller interface {
	Init(options ...BaseControllerOptions)
	InitManagers(collection *managers.Collection)
	ApplyRouter(router *echo.Group)
	RootPath() *string
}

type BaseController struct {
	BindDefaultError  irs.Coder
	DataWrapper       func(ctx echo.Context, code irs.Code, data interface{}) interface{}
	DefaultPagination generics.Pagination
}

func (r *BaseController) Init(options ...BaseControllerOptions) {
	r.BindDefaultError = irs.BadRequest
	r.DataWrapper = defaultDataWrapper
	r.DefaultPagination = defaultPagination

	for _, o := range options {
		if o.DataWrapper != nil {
			r.DataWrapper = o.DataWrapper
		}
		if o.BindDefaultError != nil {
			r.BindDefaultError = o.BindDefaultError
		}
	}
}

func (r *BaseController) InitManagers(_ *managers.Collection) {

}

func (r *BaseController) ApplyRouter(_ *echo.Group) {

}

func (r *BaseController) RootPath() *string {
	return nil
}

func (r *BaseController) Bind(ctx echo.Context, data interface{}) error {
	err := ctx.Bind(data)
	if err != nil {
		return r.BindDefaultError
	}

	if err = ctx.Validate(data); err != nil {
		return err
	}

	return nil
}

func (r *BaseController) Ok(ctx echo.Context, data interface{}, filters ...string) error {
	return r.Send(ctx, irs.Ok, data, filters...)
}

func (r *BaseController) Created(ctx echo.Context, data interface{}, filters ...string) error {
	return r.Send(ctx, irs.Created, data, filters...)
}

func (r *BaseController) NoContent(ctx echo.Context) error {
	return ctx.NoContent(http.StatusNoContent)
}

func (r *BaseController) Send(ctx echo.Context, code irs.Code, data interface{}, filters ...string) error {

	if filters == nil || len(filters) == 0 {
		filters = []string{"default"}
	} else {
		filters = append(filters, "default")
	}

	var err error
	data, err = sheriff.Marshal(&sheriff.Options{
		Groups: filters,
	}, data)
	if err != nil {
		return irsa.SendError(ctx, err)
	}

	return irsa.RawSend(ctx, code.HttpCode(), r.DataWrapper(ctx, code, data))
}
