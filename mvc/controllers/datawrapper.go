//
// filename:  datawrapper.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package controllers

import (
	"github.com/labstack/echo/v4"

	irs "gitlab.com/thebedroom/might/mvc/sender"
)

type BaseResponse struct {
	HttpCode int         `json:"httpCode" xml:"httpCode" yaml:"httpCode"`
	Message  string      `json:"message"  xml:"message"  yaml:"message"`
	Content  interface{} `json:"content"  xml:"content"  yaml:"content"`
}

func defaultDataWrapper(_ echo.Context, code irs.Code, data interface{}) interface{} {
	return &BaseResponse{
		HttpCode: code.HttpCode(),
		Message:  code.Message(),
		Content:  data,
	}
}
