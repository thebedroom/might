//
// filename:  routes.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package imvc

import (
	"strings"

	"github.com/labstack/echo/v4"
)

type routes []*echo.Route

func (r routes) Len() int {
	return len(r)
}

func (r routes) Less(i, j int) bool {
	return strings.Compare(r[i].Path, r[j].Path) < 0
}

func (r routes) Swap(i, j int) {
	tmp := r[i]
	r[i] = r[j]
	r[j] = tmp
}
