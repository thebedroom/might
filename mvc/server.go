//
// filename:  Server.go.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package imvc

import (
	"fmt"
	"log"
	"net/http"
	"sort"
	"strings"
	"sync"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"gitlab.com/thebedroom/might/mvc/controllers"
	"gitlab.com/thebedroom/might/mvc/controllers/tools"
	"gitlab.com/thebedroom/might/mvc/managers"
	"gitlab.com/thebedroom/might/mvc/repositories"
	irsa "gitlab.com/thebedroom/might/mvc/sender/api"

	"gorm.io/gorm"
)

type Server struct {
	controllerCollection *controllers.Collection
	managerCollection    *managers.Collection
	repositoryCollection *repositories.Collection

	validator       echo.Validator
	builded         bool
	Server          *echo.Echo
	NotFoundHandler func(c echo.Context) error
	addr            string
	err             error
	started         bool
}

func NewServer() *Server {
	return &Server{
		controllerCollection: controllers.NewCollection(),
		managerCollection:    managers.NewCollection(),
		repositoryCollection: repositories.NewCollection(),

		validator:       tools.DefaultStructValidator,
		builded:         false,
		Server:          echo.New(),
		NotFoundHandler: echo.NotFoundHandler,
	}
}

func (c *Server) SetControllers(controllers []controllers.Controller) {
	c.controllerCollection.Add(controllers...)
}

func (c *Server) SetManagers(managers []managers.Manager) {
	for _, v := range managers {
		c.managerCollection.Set(v)
	}
}

func (c *Server) SetRepositories(repositories []repositories.Repository) {
	for _, v := range repositories {
		c.repositoryCollection.Set(v)
	}
}

func (c *Server) Setup(db *gorm.DB) {
	c.repositoryCollection.Init(db)
	c.managerCollection.DoInit(c.repositoryCollection, c.managerCollection)
	c.controllerCollection.Init()
	c.controllerCollection.InitManagers(c.managerCollection)

	if err := c.managerCollection.DoAfterInit(); err != nil {
		log.Fatal(err)
	}
}

func (c *Server) SetCors(cfg middleware.CORSConfig) {
	c.Server.Use(middleware.CORSWithConfig(cfg))
}

func (c *Server) Build() {
	c.builded = true
	c.controllerCollection.ApplyRouter(c.Server.Group("/"))
	c.Server.Validator = c.validator
	c.Server.HTTPErrorHandler = irsa.ErrorHandler

	routes := routes(c.Server.Routes())
	sort.Sort(routes)

	log.Printf("Registered routes to listen on %s\n", c.addr)
	for _, route := range routes {
		if route.Name == "github.com/labstack/echo/v4.glob..func1" {
			continue
		}
		// Centering method
		spaces := int(float64(10-len(route.Method)) / 2)
		str := strings.Repeat(" ", spaces) + route.Method + strings.Repeat(" ", 10-(spaces+len(route.Method)))
		str = fmt.Sprintf("[%s]", str)
		str += " -> " + route.Path
		log.Println(str)
	}
}

func (c *Server) Listen(s string) error {
	if c.started {
		log.Fatal("server already started")
	}
	c.addr = s
	log.Println("Starting api on ", s)
	if err := http.ListenAndServe(s, c.Server); err != nil {
		return err
	}

	return nil
}

func (c *Server) SetAddr(addr string) {
	if c.started {
		log.Fatal("could not change addr of an already running server")
	}
	c.addr = addr
}

func (c *Server) StartNonBlocking(wg *sync.WaitGroup) {
	if c.started {
		log.Fatal("server already started")
	}
	go func() {
		c.started = true
		log.Printf("Starting server on %s\n", c.addr)
		if err := http.ListenAndServe(c.addr, c.Server); err != nil {
			c.err = err
		}
		if wg != nil {
			wg.Done()
		}
	}()
}
