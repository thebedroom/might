//
// filename:  ptrs.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package it

import (
	"time"
)

// Int takes an int as parameter and returns it as pointer
func Int(i int) *int {
	return &i
}

// Uint takes an int as parameter and returns it as pointer
func Uint(i uint) *uint {
	return &i
}

// Int8 takes an int8 as parameter and returns it as pointer
func Int8(i int8) *int8 {
	return &i
}

// Uint8 takes an uint8 as parameter and returns it as pointer
func Uint8(i uint8) *uint8 {
	return &i
}

// Int16 takes an int16 as parameter and returns it as pointer
func Int16(i int16) *int16 {
	return &i
}

// Uint16 takes an uint16 as parameter and returns it as pointer
func Uint16(i uint16) *uint16 {
	return &i
}

// Int32 takes an int32 as parameter and returns it as pointer
func Int32(i int32) *int32 {
	return &i
}

// Uint32 takes an uint32 as parameter and returns it as pointer
func Uint32(i uint32) *uint32 {
	return &i
}

// Int64 takes an int64 as parameter and returns it as pointer
func Int64(i int64) *int64 {
	return &i
}

// Uint64 takes an uint64 as parameter and returns it as pointer
func Uint64(i uint64) *uint64 {
	return &i
}

// Float32 takes a float32 as parameter and returns it as pointer
func Float32(f float32) *float32 {
	return &f
}

// Float64 takes a float64 as parameter and returns it as pointer
func Float64(f float64) *float64 {
	return &f
}

// String takes a string as parameter and returns it as pointer
func String(s string) *string {
	return &s
}

// Complex64 takes a complex64 as parameter and returns it as pointer
func Complex64(c complex64) *complex64 {
	return &c
}

// Complex128 takes a complex128 as parameter and returns it as pointer
func Complex128(c complex128) *complex128 {
	return &c
}

// Bool takes a bool as parameter and returns it as pointer
func Bool(b bool) *bool {
	return &b
}

// Time takes a time as parameter and returns it as pointer
func Time(t time.Time) *time.Time {
	return &t
}

// Duration takes a duration as parameter and returns it as pointer
func Duration(d time.Duration) *time.Duration {
	return &d
}
