package iem

import (
	"time"
)

func GetTimeRFC3339(key string) (time.Time, error) {
	v := Get(key)
	ret, err := time.Parse(time.RFC3339, v)
	if err != nil {
		return time.Time{}, err
	}
	return ret, nil
}

func GetDefaultTimeRFC3339(key string, defValue time.Time) time.Time {
	if !Has(key) {
		return defValue
	}
	v, err := GetTimeRFC3339(key)
	if err != nil {
		return defValue
	}
	return v
}

func MustGetTimeRFC3339(key string) time.Time {
	v := MustGet(key)
	ret, err := time.Parse(time.RFC3339, v)
	if err != nil {
		panic(err)
	}
	return ret
}
