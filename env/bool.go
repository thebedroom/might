//
// filename:  bool.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//
package iem

import (
	"fmt"
)

func GetBool(key string) (bool, error) {
	v := Get(key)
	switch v {
	case "1", "t", "T", "true", "TRUE", "True", "y", "Y", "yes", "Yes", "YES":
		return true, nil
	case "0", "f", "F", "false", "FALSE", "False", "n", "N", "no", "No", "NO":
		return false, nil
	default:
		return false, fmt.Errorf("parse bool error: %s", v)
	}
}

func GetDefaultBool(key string, defValue bool) bool {
	if !Has(key) {
		return defValue
	}
	v, err := GetBool(key)
	if err != nil {
		return defValue
	}
	return v
}

func MustGetBool(key string) bool {
	v := MustGet(key)
	switch v {
	case "1", "t", "T", "true", "TRUE", "True", "y", "Y", "yes", "Yes", "YES":
		return true
	case "0", "f", "F", "false", "FALSE", "False", "n", "N", "no", "No", "NO":
		return false
	default:
		panic(fmt.Errorf("parse bool error: %s", v))
	}
}
