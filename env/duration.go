package iem

import (
	"time"
)

func GetDuration(key string) (time.Duration, error) {
	v := Get(key)
	ret, err := time.ParseDuration(v)
	if err != nil {
		return 0, err
	}
	return ret, nil
}

func GetDefaultDuration(key string, defValue time.Duration) time.Duration {
	if !Has(key) {
		return defValue
	}
	v, err := GetDuration(key)
	if err != nil {
		return defValue
	}
	return v
}

func MustGetDuration(key string) time.Duration {
	v := MustGet(key)
	ret, err := time.ParseDuration(v)
	if err != nil {
		panic(err)
	}
	return ret
}
