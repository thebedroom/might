//
// filename:  json.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//
package iem

import (
	"encoding/xml"
	"fmt"
	"io"
	"os"
)

func LoadXmlEnv(file string) error {
	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()
	return LoadXmlReader(f)
}

func LoadXmlReader(reader io.Reader) error {
	bytes, err := io.ReadAll(reader)
	if err != nil {
		return err
	}
	mp := make(map[string]interface{})
	err = xml.Unmarshal(bytes, &mp)
	if err != nil {
		return err
	}
	for k, v := range mp {
		Set(k, fmt.Sprint(v))
	}
	return nil
}

func MustLoadXml(file string) {
	if err := LoadXmlEnv(file); err != nil {
		panic(err)
	}
}

func MustLoadXmlReader(reader io.Reader) {
	if err := LoadXmlReader(reader); err != nil {
		panic(err)
	}
}
