//
// filename:  scheduler.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package cron

import (
	"fmt"
	"runtime"
	"sync"
	"time"

	"github.com/go-co-op/gocron"

	it "gitlab.com/thebedroom/might/tools"
)

func init() {

}

var (
	ErrCannotRunInPast           = fmt.Errorf("cron: could not run a task in the past")
	ErrInfiniteRunWithNoInterval = fmt.Errorf("cron: could not run a task recurrently without interval")
)

type (
	JobFunc func(*Job) error

	JobOnError func(error)

	Scheduler struct {
		scheduler *gocron.Scheduler
		mtex      sync.RWMutex
		jobs      map[string]*jobDefinition
		onError   JobOnError

		cancelQueue chan *jobDefinition
		stop        chan int
	}
)

func NewScheduler() *Scheduler {
	s := &Scheduler{
		scheduler: gocron.NewScheduler(time.Local),
		mtex:      sync.RWMutex{},
		jobs:      map[string]*jobDefinition{},
		onError:   func(error) {},

		cancelQueue: make(chan *jobDefinition, runtime.NumCPU()),
		stop:        make(chan int),
	}

	return s
}

func (s *Scheduler) Start() {
	s.scheduler.StartAsync()
	s.startCanceling()

	go func() {
		select {
		case <-s.stop:
			close(s.stop)
			close(s.cancelQueue)
			s.scheduler.Stop()
			break
		}
	}()
}

func (s *Scheduler) RunEvery(id string, f JobFunc, duration time.Duration) error {
	return s.Schedule(f, &TaskDefinition{
		Id:                   id,
		Every:                it.Duration(duration),
		LimitRun:             nil,
		StartAt:              it.Time(time.Now()),
		EndsAt:               nil,
		MaxConcurrentJob:     it.Uint(1),
		MaxConcurrentJobWait: it.Bool(false),
	})
}

func (s *Scheduler) RunOnce(id string, f JobFunc, t time.Time) error {
	return s.Schedule(f, &TaskDefinition{
		Id:                   id,
		Every:                nil,
		LimitRun:             it.Uint(1),
		StartAt:              it.Time(t),
		EndsAt:               nil,
		MaxConcurrentJob:     it.Uint(1),
		MaxConcurrentJobWait: it.Bool(false),
	})
}

func (s *Scheduler) Timeout(id string, f JobFunc, timeout time.Duration) error {
	return s.Schedule(f, &TaskDefinition{
		Id:                   id,
		Every:                nil,
		LimitRun:             it.Uint(1),
		StartAt:              it.Time(time.Now().Add(timeout)),
		EndsAt:               nil,
		MaxConcurrentJob:     it.Uint(1),
		MaxConcurrentJobWait: it.Bool(false),
	})
}

func (s *Scheduler) Schedule(f JobFunc, option *TaskDefinition, options ...*TaskDefinition) error {

	option.merge(options)

	if err := option.validate(); err != nil {
		return err
	}

	s.mtex.Lock()
	defer s.mtex.Unlock()

	if option.Every != nil {
		s.scheduler.Every(*option.Every)
	} else {
		s.scheduler.Every(time.Second)
	}

	if option.StartAt != nil {
		s.scheduler.StartAt(*option.StartAt)
	}

	if option.MaxConcurrentJob != nil {
		mode := gocron.RescheduleMode
		if *option.MaxConcurrentJobWait {
			mode = gocron.WaitMode
		}
		s.scheduler.SetMaxConcurrentJobs(int(*option.MaxConcurrentJob), mode)
	}

	handler := s.makeHandler(option.Id, f)

	job, err := s.scheduler.Do(handler)
	if err != nil {
		return err
	}
	if option.LimitRun != nil {
		job.LimitRunsTo(int(*option.LimitRun))
	} else if option.EndsAt != nil {
		d := option.EndsAt.Sub(time.Now())
		limit := d / *option.Every
		job.LimitRunsTo(int(limit))
	}

	s.jobs[option.Id] = &jobDefinition{
		option:  *option,
		cronJob: job,
	}

	return nil
}

func (s *Scheduler) makeHandler(id string, f JobFunc) func() {
	return func() {
		s.mtex.RLock()
		defer s.mtex.RUnlock()

		j := s.jobs[id]
		if err := f(s.makeJob(j)); err != nil {
			s.onError(err)
		}
		if j.option.EndsAt != nil {
			if j.cronJob.NextRun().After(*j.option.EndsAt) {
				delete(s.jobs, id)
			} else if j.option.EndsAt != nil && time.Now().After(*j.option.EndsAt) {
				delete(s.jobs, id)
			}
		} else if j.option.LimitRun != nil && uint(j.cronJob.FinishedRunCount()+1) >= *j.option.LimitRun {
			delete(s.jobs, id)
		}
	}
}

func (s *Scheduler) SetOnError(e JobOnError) {
	s.onError = e
}

func (s *Scheduler) Unschedule(id string) {
	s.mtex.Lock()
	defer s.mtex.Unlock()

	j, ok := s.jobs[id]
	if !ok {
		return
	}

	delete(s.jobs, j.option.Id)

	s.cancelQueue <- j
}

func (s *Scheduler) GetById(id string) *Job {
	j, ok := s.jobs[id]
	if !ok {
		return nil
	}

	j.cronJob.NextRun()

	return s.makeJob(j)
}

func (s *Scheduler) makeJob(j *jobDefinition) *Job {
	return &Job{
		Id:           j.option.Id,
		Every:        j.option.Every,
		MaxRun:       j.option.LimitRun,
		StartsAt:     j.option.StartAt,
		EndsAt:       j.option.EndsAt,
		LastRun:      j.cronJob.LastRun(),
		NextRun:      j.cronJob.NextRun(),
		schedulerRef: s,
	}
}

func (s *Scheduler) startCanceling() {
	go func() {
		for toCancel := range s.cancelQueue {
			s.scheduler.RemoveByReference(toCancel.cronJob)
		}
	}()

}
