//
// filename:  job.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package cron

import (
	"time"

	"github.com/go-co-op/gocron"
)

type Job struct {
	Id       string
	Every    *time.Duration
	MaxRun   *uint
	StartsAt *time.Time
	EndsAt   *time.Time
	LastRun  time.Time
	NextRun  time.Time

	schedulerRef *Scheduler
}

type jobDefinition struct {
	option  TaskDefinition
	cronJob *gocron.Job
}
