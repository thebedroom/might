//
// filename:  taskdefinition.go
// author:    Thomas Lombard
// copyright: Thomas Lombard
// license:   MIT
// status:    published
//

package cron

import (
	"time"

	"github.com/google/uuid"

	it "gitlab.com/thebedroom/might/tools"
)

type TaskDefinition struct {
	Id                   string
	Every                *time.Duration
	LimitRun             *uint
	StartAt              *time.Time
	EndsAt               *time.Time
	MaxConcurrentJob     *uint
	MaxConcurrentJobWait *bool
}

func (o *TaskDefinition) merge(options []*TaskDefinition) {
	for _, option := range options {
		if option.Every != nil {
			o.Every = option.Every
		}
		if option.LimitRun != nil {
			o.LimitRun = option.LimitRun
		}
		if option.StartAt != nil {
			o.StartAt = option.StartAt
		}
		if option.EndsAt != nil {
			o.EndsAt = option.EndsAt
		}
		if option.MaxConcurrentJob != nil {
			o.MaxConcurrentJob = option.MaxConcurrentJob
		}
		if option.MaxConcurrentJobWait != nil {
			o.MaxConcurrentJobWait = option.MaxConcurrentJobWait
		}
	}
}

func (o *TaskDefinition) validate() error {
	if o.EndsAt != nil && o.EndsAt.Before(time.Now()) {
		return ErrCannotRunInPast
	}

	if o.Id == "" {
		o.Id = uuid.NewString()
	}

	if o.LimitRun == nil && o.Every == nil {
		return ErrInfiniteRunWithNoInterval
	}

	if o.MaxConcurrentJobWait == nil {
		o.MaxConcurrentJobWait = it.Bool(false)
	}

	return nil
}
